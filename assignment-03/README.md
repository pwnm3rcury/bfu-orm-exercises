# Assignment: Create the following solution using C#

## Setup Requrements:
- MS Visual Studio 2019
- C# (.NET 5.0)
- Entity Framework
- MSSQL Server

## Database Requrements:
- Create `Shop` database, with 2 tables `Product`, `Sale`.
- `Product` contains `Id, Name, Description, Price, Created, Updated` fields.
- `Sale` contains `Id, ProductId, Quantity, Client, Email, Phone, Ordered` fields.

## Project Requirements:
- Create solution using the Code-First Approach.
- Create solution using the Database-First Approach.
- Create CRUD console application as the 3rd solution in the project.