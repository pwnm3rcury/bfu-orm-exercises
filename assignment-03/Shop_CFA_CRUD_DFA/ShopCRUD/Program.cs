﻿using ShopCFA.Model;
using ShopCFA.Model.Entities;
using System;
using System.Linq;

namespace ShopCRUD
{
	class Program
	{
		static void Main(string[] args)
		{
            using (var CFADB = new ShopContext())
            {
                // CREATE ---
                var item1 = CFADB.Products.Add(new Product
                {
                    Name = "RandomItem",
                    Description = "itemdesc",
                    Price = 1,
                    Created = DateTime.Now,
                    Updated = DateTime.Now
                });

                CFADB.SaveChanges();

                var sale = CFADB.Sales.Add(new Sale
                {
                    Product = item1.Entity,
                    Quantity = 1,
                    Client = "Dimitar",
                    Email = "d@d.d",
                    Phone = "80085",
                    Ordered = DateTime.Now
                });

                CFADB.SaveChanges();

                // UPDATE ---
                var result = CFADB.Products.SingleOrDefault(b => b.ProductId == item1.Entity.ProductId);
                if (result != null)
                {
                    result.Name = "item2";
                    CFADB.SaveChanges();
                }

                // READ ---
                Console.WriteLine("Printing products:");
                foreach (var product in CFADB.Products.Select(p => p).ToList())
                {
                    Console.WriteLine(
                        "ProductId: {0}, Name: {1}, Description: {2}, Price: {3}, Created: {4}, Updated: {5}",
                        product.ProductId, product.Name, product.Description, product.Price, product.Created, product.Updated);
                }
                Console.WriteLine("Printing sales:");
                foreach (var _sale in CFADB.Sales.Select(s => s).ToList())
                {
                    Console.WriteLine("SaleId: {0}, Quantity: {1}, Client: {2}, Email: {3}, Phone: {4}, Ordered: {5}",
                        _sale.SaleId, _sale.Quantity, _sale.Client, _sale.Email, _sale.Phone, _sale.Ordered);
                }

                // DELETE ---
                CFADB.Sales.Remove(sale.Entity);
                CFADB.Products.Remove(item1.Entity);

                CFADB.SaveChanges();
            }

        }
	}
}
