CREATE TABLE [dbo].[Products] (
    [ProductId]   INT            IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (MAX) NULL,
    [Description] NVARCHAR (MAX) NULL,
    [Price]       FLOAT (53)     NOT NULL,
    [Created]     DATETIME2 (7)  NOT NULL,
    [Updated]     DATETIME2 (7)  NOT NULL,
    CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED ([ProductId] ASC)
);


CREATE TABLE [dbo].[Sales] (
    [SaleId]    INT            IDENTITY (1, 1) NOT NULL,
    [ProductId] INT            NULL,
    [Quantity]  INT            NOT NULL,
    [Client]    NVARCHAR (MAX) NULL,
    [Email]     NVARCHAR (MAX) NULL,
    [Phone]     NVARCHAR (MAX) NULL,
    [Ordered]   DATETIME2 (7)  NOT NULL,
    CONSTRAINT [PK_Sales] PRIMARY KEY CLUSTERED ([SaleId] ASC),
    CONSTRAINT [FK_Sales_Products_ProductId] FOREIGN KEY ([ProductId]) REFERENCES [dbo].[Products] ([ProductId])
);
