from P in Products 
join C in Categories on P.CategoryID equals C.CategoryID 
group P by P.Category.CategoryName into pbc 
select new { pbc.Key, AvgPrice = pbc.Average(C => C.UnitPrice) }