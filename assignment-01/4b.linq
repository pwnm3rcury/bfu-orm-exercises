from E in Employees
from O in E.Orders 
where O.ShipCountry == "Belgium"
select new { E.FirstName, E.LastName, E.Address, E.City, E.Region }
