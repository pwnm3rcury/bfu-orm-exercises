from E in Employees 
join O in Orders on E.EmployeeID equals O.EmployeeID
join C in Customers on O.CustomerID equals C.CustomerID
join S in Shippers on O.ShipVia equals S.ShipperID
where C.City == "Bruxelles" && S.CompanyName == "Speedy Express" 
select new {E.FirstName, E.LastName, C.CompanyName}