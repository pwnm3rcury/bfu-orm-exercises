from E in Employees 
orderby E.EmployeeID 
select new { E.EmployeeID, E.FirstName, E.LastName, norders = (from O in E.Orders from D in O.Order_Details select D.ProductID).Distinct().Count() }