from E1 in Employees
where E1.HireDate < (from E2 in Employees.Where ( E2 => E2.City == "London" ) select E2.HireDate).Min() 
select new { E1.FirstName, E1.LastName }