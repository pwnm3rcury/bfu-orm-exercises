from P in Products 
group P by P.CategoryID into pbc 
select new { pbc.Key, AvgPrice = pbc.Average(C => C.UnitPrice) }