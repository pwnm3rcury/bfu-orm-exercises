﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CFApp
{
	class BloggingContext : DbContext
	{

		public DbSet<Blog> Blogs { get;  set; }
		public DbSet<Post> Posts { get;  set; }

		protected override void OnConfiguring(DbContextOptionsBuilder options)
			=> options.UseSqlServer(@"Data Source=(localdb)\MSSQLLocalDB; Database=Blogging");
	}
}
