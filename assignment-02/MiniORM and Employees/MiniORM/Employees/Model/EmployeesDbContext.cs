﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Employees.Model
{
	using MiniORM;
	using Entities;

	public class EmployeesDbContext : DbContext
	{
		public EmployeesDbContext(string connectionString) : base(connectionString)
		{

		}

		public DbSet<Employee> Employees { get; }
		public DbSet<Department> Departments { get; }
		public DbSet<Project> Projects { get; }
		public DbSet<EmployeeProject> EmployeesProjects { get; }

	}
}
