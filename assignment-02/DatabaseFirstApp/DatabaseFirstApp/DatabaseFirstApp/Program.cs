﻿using DatabaseFirstApp.Model;
using System;
using System.Linq;

namespace DatabaseFirstApp
{
	class Program
	{
		static void Main(string[] args)
		{
			using (var db = new BloggingContext())
			{
				Console.WriteLine("Step 1. Create Blog ...");
				var blog = db.Blogs.Add(new Blog { Name = "My Blog" });
				db.SaveChanges();
				int blogId = blog.Entity.BlogId;

				Console.WriteLine("Step 2. Add Posts ...");
				db.Posts.Add(new Post { Title = "1", Content = "One", BlogId = blogId });
				db.Posts.Add(new Post { Title = "2", Content = "Two", BlogId = blogId });
				db.Posts.Add(new Post { Title = "3", Content = "Three", BlogId = blogId });

				db.SaveChanges();

				Console.WriteLine("Step 3. Display Posts");
				var posts = db.Posts.Where(b => b.BlogId == blogId).OrderBy(b => b.Title).ToList();
				foreach (var post in posts)
				{
					Console.WriteLine("Title: {0}, Content {1}", post.Title, post.Content);
				}
			}
		}
	}
}
