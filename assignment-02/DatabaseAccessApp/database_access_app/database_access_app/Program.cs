﻿using System;
using System.Data.SqlClient;

namespace database_access_app
{
	class Program
	{
		static void Main(string[] args)
		{
			SqlConnection dbCon = new(@"Data Source=(localdb)\MSSQLLocalDB; Database=Employees;");
			dbCon.Open();
			using (dbCon)
			{
				SqlCommand command = new("SELECT * FROM Employees", dbCon);
				SqlDataReader reader = command.ExecuteReader();
				using (reader)
				{
					while (reader.Read())
					{
						string name = (string)reader["Name"];
						string family = (string)reader["Family"];
						Console.WriteLine("{0} {1}", name, family);
					}
				}
			}
		}
	}
}
